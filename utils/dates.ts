const FORMATTER = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: 'long',
});


export function formatRange(startDate: Date, endDate?: Date) {
    if (!endDate) {
        return `${FORMATTER.format(startDate)} - current`;
    }
    return FORMATTER.formatRange(startDate, endDate);
}