import classNames from "classnames";
import Script from "next/script";

import Text from "../Text";
import { ComponentPropsWithoutRef } from "react";


const ICON_TYPES = {
    solid: 'solid',
    brands: 'brands',
} as const;

const ICON_SIZES = {
    xs: 'body',
    sm: 'h6',
    md: 'h5',
    lg: 'h4',
    xl: 'h3',
} as const;

type IconType = typeof ICON_TYPES[keyof typeof ICON_TYPES];
type IconSize = keyof typeof ICON_SIZES;
type IconColor = ComponentPropsWithoutRef<typeof Text<'i'>>['fontColor'];

interface IconProps {
    icon: string;
    type: IconType;
    size?: IconSize;
    color?: IconColor;
}

export default function Icon({ icon, type, size = 'md', color, ...other }: IconProps & ComponentPropsWithoutRef<'i'>) {
    const className = classNames(`fa-${type}`, `fa-${icon}`);
    return (
        <>
            <Script src="https://kit.fontawesome.com/f4b4faa5ce.js" crossOrigin="anonymous" />
            <Text as="i" className={className} fontType={ICON_SIZES[size]} fontColor={color} {...other} />
        </>
    )
}