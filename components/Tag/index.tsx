import { ReactNode } from "react";
import styles from './styles.module.css';
import classNames from "classnames";

const TAG_TYPES = {
    primary: 'primary',
    secondary: 'secondary',
    tertiary: 'tertiary',
} as const;

type TagType = keyof typeof TAG_TYPES;

interface TagProps {
    children: string;
    type: TagType;
}

export default function Tag({ children, type }: TagProps) {
    const className = classNames(styles.wrapper, styles[type]);
    return (
        <div className={className}>
            {children}
        </div>
    )
}