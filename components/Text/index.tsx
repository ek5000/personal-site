import classNames from "classnames";
import { ComponentPropsWithoutRef, ElementType, ReactElement, ReactHTMLElement, ReactNode } from "react";
import styles from './styles.module.css';

const FONT_TYPES = {
    body: 'body',
    h1: 'h1',
    h2: 'h2',
    h3: 'h3',
    h4: 'h4',
    h5: 'h5',
    h6: 'h6',
} as const;

const FONT_COLORS = {
    primary: 'primary',
    secondary: 'secondary',
    tertiary: 'tertiary',
    light: 'light',
    dark: 'dark',
    textOnPrimary: 'textOnPrimary',
    textOnSecondary: 'textOnSecondary',
} as const;


type FontType = typeof FONT_TYPES[keyof typeof FONT_TYPES];
type FontColor = typeof FONT_COLORS[keyof typeof FONT_COLORS];

export interface TextProps<T extends ElementType> {
    children?: ReactNode;
    as?: T;
    fontType?: FontType;
    fontColor?: FontColor;
    className?: string;
}

export default function Text<T extends ElementType = 'p'>({ children, as, fontType, fontColor, className: classNameProp, ...props }: TextProps<T> & Omit<ComponentPropsWithoutRef<T>, keyof TextProps<T>>) {
    const Component = as ?? 'p';
    const className = classNames(classNameProp, styles.text, fontType && styles[fontType], fontColor && styles[fontColor]);
    return <Component className={className} {...props}>{children}</Component>
}