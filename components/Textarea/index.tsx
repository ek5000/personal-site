import classNames from "classnames";
import { TextareaHTMLAttributes } from "react";

import styles from './styles.module.css';


export default function Textarea({ className, ...props}: TextareaHTMLAttributes<HTMLTextAreaElement>) {
    const finalClassName = classNames(className, styles.wrapper);
    return <textarea className={finalClassName} {...props} />;
}