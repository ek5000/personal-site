import classNames from "classnames";
import { InputHTMLAttributes } from "react";

import styles from './styles.module.css';


export default function Input({ className, ...props}: InputHTMLAttributes<HTMLInputElement>) {
    const finalClassName = classNames(className, styles.wrapper);
    return <input className={finalClassName} {...props} />;
}