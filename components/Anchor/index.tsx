import { ElementType } from "react";
import Text, { TextProps } from "../Text";
import Link from "next/link";
import styles from './styles.module.css';
import classNames from "classnames";

interface AnchorProps {
    isInternal?: boolean;
    href: string;
    className?: string;
    children: string;
}

export default function Anchor<T extends ElementType = 'a'>({ isInternal = true, href, className, ...props }: AnchorProps & Omit<TextProps<T>, 'as'>) {
    const Component = isInternal ? Link : 'a';
    return (
        <Component href={href}>
            <Text className={classNames(styles.anchor, className)} {...props}/>
        </Component>
    );
}