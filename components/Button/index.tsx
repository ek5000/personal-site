import { ComponentPropsWithoutRef, ElementType, ReactNode } from "react";
import classNames from "classnames";
import styles from './styles.module.css';

const BUTTON_TYPES = {
    primary: 'primary',
    secondary: 'secondary',
};

const BUTTON_SIZES = {
    sm: 'sm',
    md: 'md',
    lg: 'lg',
} as const;

type ButtonSize = typeof BUTTON_SIZES[keyof typeof BUTTON_SIZES];
type ButtonType = typeof BUTTON_TYPES[keyof typeof BUTTON_TYPES];

interface ButtonProps<T extends ElementType> {
    children: ReactNode;
    buttonType: ButtonType;
    as?: T;
    className?: string;
    size?: ButtonSize;
}

export default function Button<T extends ElementType = 'button'>({ buttonType = BUTTON_TYPES.primary, as, size, className: classNameProp, children, ...props }: ButtonProps<T> & Omit<ComponentPropsWithoutRef<T>, keyof ButtonProps<T>>) {
    const Component = as ?? 'button';
    const className = classNames(classNameProp, styles.button, styles[buttonType], size && styles[size]);
    return <Component {...props} className={className}>{children}</Component>;
}