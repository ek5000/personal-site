import polyfill from '@oddbird/css-anchor-positioning/fn';
import { useQuery } from '@tanstack/react-query';

export function useCssAnchorPolyfill() {
    return useQuery({
        queryKey: ['css-anchor-polyfill'],
        queryFn() {
            return polyfill();
        },
    })
}