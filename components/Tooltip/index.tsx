'use client';

import Text from "@/components/Text";
import { useCssAnchorPolyfill } from "./utils";
import React, { CSSProperties, ComponentPropsWithRef, ElementType, ReactElement, ReactNode, useCallback, useRef } from "react";
import classNames from "classnames";
import styles from './styles.module.css';

type EligibleTooltip = ElementType<{ id: string, className: string}>;
type EligibleTooltipProps = ComponentPropsWithRef<EligibleTooltip>;

interface TooltipProps{
    children: ReactElement<EligibleTooltipProps>;
    id: string;
    label: string;
    highlightColor?: string;
}

interface TriggerStyle extends CSSProperties {
    '--highlight-color'?: string;
}

export default function Tooltip({ label, children, highlightColor, id }: TooltipProps) {
    const { isFetched } = useCssAnchorPolyfill();
    const popoverRef = useRef<HTMLElement>(null);
    const onShowPopover = useCallback(() => {
        if (!isFetched) {
            return;
        }
        if (!popoverRef.current) {
            return;
        }
        popoverRef.current.showPopover();
    }, [isFetched]);
    const onHidePopover = useCallback(() => {
        if (!isFetched) {
            return;
        }
        if (!popoverRef.current) {
            return;
        }
        popoverRef.current.hidePopover();
    }, [isFetched]);

    const triggerId = `${id}-trigger`;
    const popoverId = `${id}-popover`;
    const triggerStyle: TriggerStyle = { '--highlight-color': highlightColor };
    const clonedChildren = React.cloneElement(children, {
        className: classNames(styles.popover, children.props.className),
        id: popoverId,
        ref: popoverRef,
        popover: 'auto',
        anchor: triggerId,
    });
    return (
        <>
            <Text id={triggerId} popoverTarget={isFetched ? popoverId : undefined} as="button" className={styles.trigger} onMouseEnter={onShowPopover} onTouchStart={onShowPopover} onMouseLeave={onHidePopover} onTouchEnd={onHidePopover} style={triggerStyle}>
                {label}
            </Text>
            {clonedChildren}
        </>
    )
}