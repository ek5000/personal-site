import Text from "@/components/Text";
import Form from "./_components/Form";
import styles from './styles.module.css';


export default function Contact() {
    return (
        <div className={styles.wrapper}>
            <Text as="h2" fontType="h3">Let's build together</Text>
            <Form />
        </div>
    )
}