'use client';

import { Form, Field, FormSpy } from 'react-final-form'
import Button from "@/components/Button";
import { FormInputs, useOnSubmit } from './utils';
import Input from '@/components/Input';

import styles from './styles.module.css';
import Text from '@/components/Text';
import Textarea from '@/components/Textarea';

export default function ContactForm() {
    const { mutateAsync } = useOnSubmit();
    return (
        <Form<FormInputs>

            onSubmit={values => mutateAsync(values)}
            render={({ handleSubmit, form }) => {
                return (
                    <form onSubmit={handleSubmit} className={styles.wrapper}>
                        <Field
                            name="name"
                            render={({ input }) => {
                                return (
                                    <div className={styles.inputWrapper}>
                                        <Text as="label" htmlFor="name">Name</Text>
                                        <Input type="text" id="name" placeholder="Name" required {...input} />
                                    </div>
                                );
                            }}
                        />
                        <Field
                            name="email"
                            render={({ input }) => {
                                return (
                                    <div className={styles.inputWrapper}>
                                        <Text as="label" htmlFor="email">Email</Text>
                                        <Input type="text" id="email" placeholder="Email" required {...input} />
                                    </div>
                                );
                            }}
                        />
                        <Field
                            name="message"
                            render={({ input }) => {
                                return (
                                    <div className={styles.inputWrapper}>
                                        <Text as="label" htmlFor="message">Message</Text>
                                        <Textarea id="message" placeholder="Message" required {...input} />
                                    </div>
                                );
                            }}
                        />
                        <Field
                            name="botcheck"
                            type="checkbox"
                            render={({ input }) => {
                                return <Input type="checkbox" style={{ display:  'none' }} {...input} />;
                            }}
                        />
                        <FormSpy subscription={{ submitSucceeded: true }} onChange={({ submitSucceeded }) => {
                            if (submitSucceeded) {
                                form.reset();
                            }
                        }}
                        />

                        <Button buttonType="primary" type="submit" size="sm">Submit</Button>
                    </form>
                )
            }}
        />
    )
}