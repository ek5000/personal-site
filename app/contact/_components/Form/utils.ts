import { useMutation } from "@tanstack/react-query";
import { z } from "zod";

const WEB_3_FORMS_URL = 'https://api.web3forms.com/submit';
const ACCESS_KEY = '2ed6f0d0-232c-4298-bbf6-f409f8ed250e';
const MAX_NAME_LENGTH = 64;
const FORM_SCHEMA = z.object({
    name: z.string().max(MAX_NAME_LENGTH),
    email: z.string().email(),
    message: z.string(),
    botcheck: z.boolean(),
});

export type FormInputs = z.infer<typeof FORM_SCHEMA>;

export function useOnSubmit() {
    return useMutation<boolean, unknown, FormInputs>({
        async mutationFn(variables) {
            const body = {
                ...variables,
                access_key: ACCESS_KEY,
            };
            const response = await fetch(WEB_3_FORMS_URL, {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            const responseJson = await response.json();
            if (responseJson.success) {
                return true;
            }
            throw new Error('Submit failed');
        },
    })
}