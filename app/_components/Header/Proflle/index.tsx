import Image from 'next/image';
import profilePic from '../../../../public/me.jpg';
import styles from './styles.module.css';
import Text from '@/components/Text';
import Link from 'next/link';

export default function Profile() {
    return (
        <Link className={styles.wrapper} href="/">
            <Image src={profilePic} alt="" width={40} height={40} className={styles.image} />
            <Text fontColor="textOnSecondary" className={styles.name}>Ekongi "E.K." Yancy</Text>
        </Link>
    )
}