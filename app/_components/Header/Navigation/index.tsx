import ContactButton from "./ContactButton";
import Hamburger from "./Hamburger";
import NavLinks from "./NavLinks";
import styles from './styles.module.css';

const LINKS = [
    { label: 'About', path: '/about' },
    { label: 'Resume', path: '/resume' },
] as const;

export type Links = typeof LINKS;

export default function Navigation() {
    return (
        <>
            <div className={styles.navLinks}>
                <NavLinks links={LINKS} />
                <ContactButton />
            </div>
            <Hamburger className={styles.hamburger} links={LINKS}/>
        </>
    )
}