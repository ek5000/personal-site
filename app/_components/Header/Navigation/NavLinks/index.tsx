'use client'

import classNames from "classnames";
import NavLink from "./NavLink";
import styles from './styles.module.css';
import { Links } from "..";

interface NavLinksProps {
    className?: string;
    readonly links: Links;
}

export default function NavLinks({ className, links }: NavLinksProps) {
    const finalClassName = classNames(className, styles.list);
    return (
        <ul className={finalClassName}>
            {links.map(({ label, path }) => (
                <li>
                    <NavLink path={path} label={label} />
                </li>
            ))}
        </ul>
    )
}