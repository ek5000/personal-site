'use client'

import Link from "next/link";
import { usePathname } from "next/navigation";
import cx from 'classnames';
import styles from './styles.module.css';
import Text from "@/components/Text";

interface NavLinkProps {
    label: string;
    path: string;
}

export default function NavLink({ label, path }: NavLinkProps) {
    const pathname = usePathname();
    const className = cx(
        styles.link,
        pathname === path && styles.active,
    );
    return (
        <Text as={Link} href={path} className={className} fontType="body" fontColor="textOnSecondary">
            {label}
        </Text>
    )
}