import Text from "@/components/Text";
import Link from "next/link";
import { Links } from "../..";
import styles from './styles.module.css';

interface MenuProps {
    links: Links;
    onLinkClick: () => void;
}

export default function Menu({ links, onLinkClick }: MenuProps) {
    const other = links.slice(0, -1);
    const last = links.at(-1);
    return (
        <div className={styles.wrapper}>
            {other.map(({ label, path }) => (
                <>
                    <Text as={Link} href={path} fontType="body" fontColor="textOnPrimary" className={styles.link} onClick={onLinkClick}>
                        {label}
                    </Text>
                    <div className={styles.divider} />
                </>
            ))}
            {last && (
                <Text as={Link} href={last.path} fontType="body" fontColor="textOnPrimary" className={styles.link} onClick={onLinkClick}>
                    {last.label}
                </Text>
            )}
        </div>
    )
}