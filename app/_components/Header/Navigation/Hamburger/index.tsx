'use client';

import Icon from "@/components/Icon";
import classNames from "classnames";
import { useState } from "react";
import { Links } from "..";
import Menu from "./Menu";
import styles from './styles.module.css';

interface HamburgerProps {
    className?: string;
    links: Links;
}

export default function Hamburger({ className, links }: HamburgerProps) {
    const [isOpen, setIsOpen] = useState(false);
    const buttonClassName = classNames(styles.button, className);
    return (
        <>
            <button className={buttonClassName} onClick={() => setIsOpen(value => !value)}>
                <Icon icon={isOpen ? 'x' : 'bars'} type="solid" />
            </button>
            {isOpen && <Menu links={links} onLinkClick={() => setIsOpen(false)} />}
        </>
    ) 
}