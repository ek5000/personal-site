import Button from "@/components/Button";
import Link from "next/link";
import styles from './styles.module.css';

export default function ContactButton() {
    return (
        <Button buttonType="primary" size="sm" as={Link} className={styles.wrapper} href="/contact">Contact</Button>
    )
}