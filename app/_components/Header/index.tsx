import Navigation from "./Navigation";
import Profile from "./Proflle";

import styles from './styles.module.css';

export default function Header() {
    return (
        <header className={styles.wrapper}>
            <Profile />
            <nav className={styles.links}>
                <Navigation />
            </nav>
        </header>
    )
}