import Text from '@/components/Text';
import styles from './styles.module.css';

export default function Body() {
    return (
        <div className={styles.wrapper}>
            <Text fontType="h2" fontColor="textOnSecondary">Hey there, I'm E.K.</Text>
            <Text fontColor="textOnSecondary" fontType="h6">
                Web developer with over 10 years of expereince who loves building
                {' '}
                <Text as="span" fontColor="primary">fast</Text>
                {' '}
                and
                {' '}
                <Text as="span" fontColor="primary">scalable</Text>
                {' '}
                UIs and APIs.
            </Text>
        </div>
    );
}