import Icon from "@/components/Icon";

import styles from './styles.module.css';

export default function Footer() {
    return (
        <footer className={styles.wrapper}>
            <a href="https://gitlab.com/ek5000">
                <Icon type="brands" icon="gitlab" color="secondary" />
            </a>
            <a href="mailto:ekongi@yancy.dev">
                <Icon type="solid" icon="envelope" color="secondary"  />
            </a>
        </footer>
    )
}