import Text from '@/components/Text';
import styles from './styles.module.css';
import Tooltip from '@/components/Tooltip';
import Image from 'next/image';
import utWordmark from '../../../../public/ut-wordmark.svg';
import quakeRugbyLogo from '../../../../public/quake-logo.webp';

export default function AboutMe() {
    return (
        <div className={styles.wrapper}>
            <Text>
                I'm a fullstack software developer, with a focus on React, GraphQL and Typescript.
            </Text>
            <Text as="div">
                In 2006 at a summer camp I fell in love with programming.
                {' '}
                I went on to make my passion my career, studying computer science in high school and
                {' '}
                <Tooltip id="about-me-hookem" label="college" highlightColor="#bf5700">
                    <Image alt="University of Texas at Austin" height={40} src={utWordmark} className={styles.hookemPopover} />
                </Tooltip>
                .
            </Text>
            <Text>
                Nowadays, I continue to follow my passion for building both front-ends and back-ends. In particular,
                {' '}
                I love tackling complex problems and simplifying them into their base components, and
                {' '}
                building those components in a way that is scalable and maintainable.
                {' '}
                Nothing brings me more satisfaction than coming back to a project months, or years later, and
                {' '}
                finding it is still easy to understand and iterate on.
            </Text>
            <Text>
                My other coding passion is mentorship. Whether it's members of my own team, or a client I'm working
                {' '}
                with, I strive to teach software engineering principles.
            </Text>
            <Text>
                When I'm not coding, you can often find me on a
                {' '}
                <Tooltip id="about-me-quake" label="rugby pitch" highlightColor='#18592d'>
                    <Image alt="Seattle Quake Rugby" height={40} src={quakeRugbyLogo} className={styles.quakePopover} />
                </Tooltip>
                {' '}
                or traveling around the world.
            </Text>
        </div>
    )
}