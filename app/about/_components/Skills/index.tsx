import Skill from "./Skill";
import styles from './styles.module.css';

export default function Skills() {
    return (
        <div className={styles.wrapper}>
            <Skill label="React" score="xl" />
            <Skill label="Javascript" score="xl" />
            <Skill label="Java" score="lg" />
            <Skill label="HTML/CSS" score="lg" />
            <Skill label="Typescript" score="lg" />
            <Skill label="GraphQL" score="lg" />
            <Skill label="Node.JS" score="lg" />
            <Skill label="Python" score="md" />
            <Skill label="C#" score="md" />
            <Skill label="Docker" score="sm" />
        </div>
    )
}