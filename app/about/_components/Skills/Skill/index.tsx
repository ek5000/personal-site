'use client';

import { CSSProperties, useRef } from 'react';
import styles from './styles.module.css';
import { useIntersection } from 'react-use';
import classNames from 'classnames';

const SCORE_PERCENTAGES = {
    xs: '20%',
    sm: '40%',
    md: '60%',
    lg: '80%',
    xl: '100%',
} as const;

type Score = keyof typeof SCORE_PERCENTAGES;

interface SkillProps {
    label: string;
    score: Score;
}

interface ProgressStyle extends CSSProperties {
    '--score'?: string;
}


export default function Skill({ label, score }: SkillProps) {
    const ref = useRef<HTMLDivElement>(null);
    const intersection = useIntersection(ref, {
        root: null,
        rootMargin: '0px',
        threshold: 1,
    });
    const isVisible = intersection && intersection.intersectionRatio >= 1;
    const progressStyle = { '--score': SCORE_PERCENTAGES[score] } as ProgressStyle;
    const className = classNames(styles.wrapper, isVisible && styles.visible);
    return (
        <div className={className} ref={ref}>
            <div className={styles.label}>
                {label}
            </div>
            <div className={styles.progressWrapper}>
                <div className={styles.progress} style={progressStyle} />
            </div>
        </div>
    )
}