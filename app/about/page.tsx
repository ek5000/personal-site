import Text from '@/components/Text';
import AboutMe from './_components/AboutMe';
import styles from './styles.module.css';
import Skills from './_components/Skills';

export default function About() {
    return (
        <div className={styles.wrapper}>
            <div className={styles.aboutMe}>
                <Text as="h2" fontType="h3">About</Text>
                <AboutMe />
            </div>
            <div className={styles.skills}>
                <Text as="h2" fontType="h3">Skills</Text>
                <Skills />
            </div>
        </div>
    )
}