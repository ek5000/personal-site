import type { Metadata } from "next";
import { Inter } from "next/font/google";
import './normalize.css';
import "./globals.css";
import styles from './layout.module.css';
import classNames from "classnames";
import Header from "./_components/Header";
import Providers from "./_components/Providers";
import Footer from "./_components/Footer";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Ekongi Yancy - Full-stack developer",
  description: "My own personal corner of the internet",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const bodyClassNames = classNames(inter.className, styles.body);
  return (
    <Providers>
      <html lang="en">
        <body className={bodyClassNames}>
          <Header />
          <main className={styles.main}>
            {children}
          </main>
          <Footer />
        </body>
      </html>
    </Providers>
  );
}
