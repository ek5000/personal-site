import Text from '@/components/Text';
import styles from './styles.module.css';
import Skills from './Skills';
import { formatRange } from '@/utils/dates';
interface SectionProps {
    company: string;
    title: string;
    details: string[];
    startDate: Date;
    endDate?: Date;
    skills: string[];
}

export default function Section({ company, title, details, skills, startDate, endDate }: SectionProps) {
    return (
        <div className={styles.wrapper}>
            <Text as="h2" fontType='h6' fontColor='tertiary'>
                {company}
            </Text>
            <div className={styles.title}>
                <Text fontColor="textOnSecondary" as="b">
                    {title}
                </Text>
                <Text as="i">
                    {formatRange(startDate, endDate)}
                </Text>
            </div>
            <ul>
            {details.map(detail => (
                <Text key={detail} className={styles.detailItem} as="li">
                    {detail}
                </Text>
            ))}
            </ul>
            <Skills skills={skills} />
        </div>
    )
}