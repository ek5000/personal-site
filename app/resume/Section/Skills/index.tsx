import Tag from '@/components/Tag';
import styles from './styles.module.css';

interface SkillsProps {
    skills: string[];
}

export default function Skills({ skills }: SkillsProps) {
    return (
        <div className={styles.wrapper}>
            {skills.map(skill => <Tag key={skill} type="secondary">{skill}</Tag>)}
        </div>
    )
}