import Section from "./Section";
import styles from './styles.module.css';

export default function Resume() {
    return (
        <ul className={styles.wrapper}>
            <li className={styles.section}>
                <Section
                    company="Zillow Group"
                    title="Principal Software Development Engineer"
                    details={
                        [
                            'Created a standalone graphql service federated into the company-wide graph',
                            'Implemented an AB testing framework and pioneered a new way of calling it by unique user',
                            'Presented multiple talks at a tech conference',
                            'Built a reuseable library for multi-step forms',
                            'Created a rich PDF generation framework using React',
                        ]
                    }
                    skills={['java', 'javascript', 'typescript', 'python', 'react', 'graphql', 'pdf']}
                    startDate={new Date('2018-12-03')}
                    endDate={new Date('2024-05-17')}
                />
            </li>
            <li className={styles.section}>
                <Section
                    company="Energysavvy"
                    title="Software Development Engineer"
                    details={
                        [
                            'Drove recreation of a utility customer facing survey using React, entirely driven by meta',
                            'Split a monolith application into microservices, including surveys, emails, and admin',
                            'Created an interface for a third-party vendor to mail surveys and return results',
                            'Instrumented multiple tools to help with front-end development, including eslint, stylelint, babel, webpack, rollup and react-scripts'
                        ]
                    }
                    skills={['python', 'javascript', 'react', 'webpack']}
                    startDate={new Date('2015-12-07')}
                    endDate={new Date('2018-11-16')}
                />
            </li>
            <li className={styles.section}>
                <Section
                    company="Amazon"
                    title="Software Development Engineer"
                    details={
                        [
                            'Created backend for scheduling website which communicates with Exchange and Active Directory',
                            'Implemented Node.JS server for frontend, serving an Angular.JS web app',
                            'Developed credential rotation algorithm for service accounts',
                        ]
                    }
                    skills={['java', 'javascript', 'python', 'angular']}
                    startDate={new Date('2014-07-07')}
                    endDate={new Date('2015-11-27')}
                />
            </li>
            <li className={styles.section}>
                <Section
                    company="Intel"
                    title="Software Developer Intern"
                    details={
                        ['Created an anonymous usage data reporting service, utilizing hadoop to analyze']
                    }
                    skills={['java', 'python', 'hadoop']}
                    startDate={new Date('2013-06-03')}
                    endDate={new Date('2014-05-23')}
                />
            </li>
        </ul>
    )
}